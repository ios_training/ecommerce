//
//  ProductInteractor.swift
//  E-Commerce
//
//  Created by Admin on 11/26/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation

protocol ProductInteractorProtocol {
    func getProducts(resultData:DataCallResult)
}
