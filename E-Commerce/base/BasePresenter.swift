//
//  BasePresenter.swift
//  E-Commerce
//
//  Created by Admin on 11/25/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation

class BasePresenter {
    
    var baseVC:BaseVCProtocol
    
    init(baseVC:BaseVCProtocol) {
        self.baseVC = baseVC
    }
    
}
