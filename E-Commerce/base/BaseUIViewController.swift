//
//  BaseUIViewController.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation
import UIKit


class BaseUIViewController: UIViewController,BaseVCProtocol {

    
    
    override func viewDidLoad() {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor(red: 41/255, green: 171/255, blue: 249/255, alpha: 1.0)
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }
    
    func startLoading() {
        
    }
    
    func endLoading() {
        
    }
    
    func success(item: Any) {
        
    }
    
    func error(msg: String) {
        
    }
}
