//
//  CellProtocol.swift
//  E-Commerce
//
//  Created by Admin on 11/23/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation

protocol CellProtocol {
    func click(index:Int)
}
