//
//  HomePresenter.swift
//  E-Commerce
//
//  Created by Admin on 11/26/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation

class HomePresenter :BasePresenter,DataCallResult {
    
    var interactor:ProductInteractorProtocol?
    
    override init(baseVC:BaseVCProtocol) {
        super.init(baseVC: baseVC)
        interactor = DataCall()
    }
    

    func getProducts(){
        baseVC.startLoading()
        interactor?.getProducts(resultData: self)
    }
    
    func success(t: Any) {
        baseVC.endLoading()
        baseVC.success(item: t)
    }

    func error(error: String) {
        baseVC.endLoading()
        baseVC.error(msg: error)
    }
    
    
}
