//
//  LoginPresenter.swift
//  E-Commerce
//
//  Created by Admin on 11/25/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation
import Firebase

class LoginPresenter :BasePresenter{
    
    override init(baseVC:BaseVCProtocol) {
        super.init(baseVC: baseVC)
    }
    
    func signIn(email:String,password:String)  {
        if !Validation.isValidEmail(testStr: email){
            baseVC.error(msg: "Please Enter Valid Email")
        }else if !Validation.isValidPassword(password: password){
            baseVC.error(msg: "Please Enter Valid Password")
        }else{
            baseVC.startLoading()
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if let currentUser = user?.user{
                    self.baseVC.success(item: currentUser.email)
                    self.baseVC.endLoading()
                }else{
                    self.baseVC.error(msg:(error?.localizedDescription)!)
                    self.baseVC.endLoading()
                }
            }
        }
        
    }
}
