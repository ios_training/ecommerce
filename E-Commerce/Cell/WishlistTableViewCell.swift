//
//  WishlistTableViewCell.swift
//  E-Commerce
//
//  Created by Admin on 11/23/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import UIKit

class WishlistTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    var cellProtocol:CellProtocol? = nil
    var index:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(product:Product) {
        nameLabel.text = product.name
        priceLabel.text = product.price
        imageLabel.image = UIImage(named:product.image)
    }
    
    @IBAction func remove(_ sender: Any) {
        if let cell = cellProtocol{
            cell.click(index: index)
        }
    }
}
