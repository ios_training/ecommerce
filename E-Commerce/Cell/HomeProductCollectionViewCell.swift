//
//  HomeProductCollectionViewCell.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import UIKit
import SDWebImage

class HomeProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
    }
    
    
    @IBAction func addToCart(_ sender: Any) {
    }
    
    func setData(product:Product) {
        name.text = product.name
        price.text = product.price
        image.image = UIImage(named: product.image)
        image.sd_setImage(with: URL(string: product.image))
    }
}
