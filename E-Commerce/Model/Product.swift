//
//  Product.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation
import ObjectMapper

struct Product : Mappable {
    
    
    var id = 0
    var name = ""
    var image = ""
    var price = ""
    var added = false
    
    init?(map: Map) {
    
    }
    
    mutating func mapping(map: Map) {
        id     <- map["id"]
        name   <- map["name"]
        image  <- map["photo"]
        price  <- map["price"]
    }
    
    
    
    init(name:String,image:String,price:String,added:Bool) {
        self.name = name;
        self.image = image;
        self.price = price;
        self.added = added;
    }
  
}
