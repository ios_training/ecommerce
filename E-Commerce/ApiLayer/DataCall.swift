//
//  DataCall.swift
//  E-Commerce
//
//  Created by Admin on 11/26/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation
import  Alamofire
import ObjectMapper


class DataCall :ProductInteractorProtocol{
    
    
    func getProducts(resultData:DataCallResult)  {
        
        Alamofire.request(ApiConfig.BASE_URL+"index.json", method: .get).responseJSON { response in
            switch response.result {
            case .success:
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    let productList = Mapper<Product>().mapArray(JSONString: utf8Text)
                    resultData.success(t: productList)
                }
                break
            case .failure:
                if let err = response.result.error{
                    resultData.error(error: err.localizedDescription)
                }
                break
            }
        }
    }
 
}
