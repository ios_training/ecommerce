//
//  DataCallResult.swift
//  E-Commerce
//
//  Created by Admin on 11/26/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation

protocol DataCallResult {
    func success(t:Any)
    func error(error:String)
}
