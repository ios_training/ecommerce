//
//  Validation.swift
//  E-Commerce
//
//  Created by Admin on 11/25/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation

class Validation {
    
    static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidPassword(password:String) -> Bool {
        return password.count >= 6
    }
}
