//
//  CustomUITextField.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class CustomUITextField : UITextField {
    
  
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            self.clipsToBounds = true
            layer.cornerRadius = cornerRadius
        }
    }
    
}
