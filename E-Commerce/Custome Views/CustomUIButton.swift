//
//  CustomUIButton.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class CustomUIButton : UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
