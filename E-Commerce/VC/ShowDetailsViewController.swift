//
//  ShowDetailsViewController.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import UIKit
import SDWebImage

class ShowDetailsViewController: UIViewController {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    
    var product:Product?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setData() {
        if let item = product {
            image.image = UIImage(named: item.image)
            nameLabel.text = item.name
            priceLabel.text = item.price
            image.sd_setImage(with: URL(string: item.image))
        }
    }
}
