//
//  WishlistViewController.swift
//  E-Commerce
//
//  Created by Admin on 11/23/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import UIKit

class WishlistViewController: BaseUIViewController,UITableViewDelegate,UITableViewDataSource,CellProtocol {
   
    
    var lists = [Product]()
 
    @IBOutlet weak var wishlistTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setData()
        wishlistTable.delegate = self
        wishlistTable.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wishlist", for: indexPath) as! WishlistTableViewCell
        cell.setData(product: lists[indexPath.row])
        cell.index = indexPath.row
        cell.cellProtocol = self
        return cell
    }
    
    func click(index:Int) {
        lists.remove(at: index)
        wishlistTable.deleteRows(at: [IndexPath(item: index, section: 0)], with: .fade)
    }
    
    func setData()  {
        lists.append(Product(name: "Quartz Wristwatch", image: "p1", price: "220$", added: false))
        lists.append(Product(name: "Sport Watch Black LED", image: "p2", price: "249$", added: false))
    }
}
