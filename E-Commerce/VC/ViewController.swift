//
//  ViewController.swift
//  E-Commerce
//
//  Created by Admin on 11/21/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: BaseUIViewController,UITextFieldDelegate{
   
    @IBOutlet weak var etUserName: UITextField!
    @IBOutlet weak var etPassword: UITextField!
    @IBOutlet weak var errorMsgLabel: CustomUILabel!
    
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var presenter:LoginPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        etUserName.delegate = self
        etPassword.delegate = self
        presenter = LoginPresenter(baseVC: self)
        indicator.stopAnimating()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        etUserName.resignFirstResponder()
        etPassword.resignFirstResponder()
        return true
    }
    @IBAction func signinAction(_ sender: Any) {
        presenter?.signIn(email: etUserName!.text!, password: etPassword!.text!)
    }

    
}

