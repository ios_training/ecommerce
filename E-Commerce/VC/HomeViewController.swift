//
//  HomeViewController.swift
//  E-Commerce
//
//  Created by Admin on 11/22/18.
//  Copyright © 2018 Beinno. All rights reserved.
//

import UIKit

class HomeViewController: BaseUIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
 
    @IBOutlet weak var collection: UICollectionView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var lists = [Product]()
    var presenter:HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        collection.dataSource = self
        collection.delegate = self
        presenter = HomePresenter(baseVC: self)
        presenter?.getProducts()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product_cell", for: indexPath) as! HomeProductCollectionViewCell
        let item = lists[indexPath.row]
        cell.setData(product: item)
        return  cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "show_details"
        {
            if let index =  collection.indexPathsForSelectedItems{
                let vc = segue.destination as? ShowDetailsViewController
                vc?.product = lists[index[0].row]
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "show_details", sender: self)
    }
    
    override func startLoading() {
        spinner.startAnimating()
        collection.isHidden = true
    }
    override func endLoading() {
        spinner.stopAnimating()
        collection.isHidden = false
    }
    override func success(item: Any) {
        lists = item as! [Product]
        collection.reloadData()
    }
    override func error(msg: String) {
        
    }
    
}
